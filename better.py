import cv2
image = cv2.imread('face.png')
frame = image
face_detection = cv2.CascadeClassifier('/Users/tonami/Downloads/opencv-master/data/haarcascades/haarcascade_frontalface_default.xml')
leye_detection = cv2.CascadeClassifier('/Users/tonami/Downloads/opencv-master/data/haarcascades/haarcascade_lefteye_2splits.xml')
nose_detection = cv2.CascadeClassifier('/Users/tonami/Downloads/opencv-master/data/haarcascades/haarcascade_mcs_nose.xml')
mouth_detection = cv2.CascadeClassifier('/Users/tonami/Downloads/opencv-master/data/haarcascades/haarcascade_mcs_mouth.xml')


gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

#threshold using random numbers until the number satisfies
faces = face_detection.detectMultiScale(gray, 1.1, 5)
leyes = leye_detection.detectMultiScale(gray, 1.15, 3)
noses = nose_detection.detectMultiScale(gray, 2, 5)
mouths = mouth_detection.detectMultiScale(gray, 2, 15)

crop_images = []

eyes_1 = []
x_eyes = []
y_eyes = []
w_eyes = []
h_eyes = []

nose = []
x_nose = []
y_nose = []
w_nose = []
h_nose = []

mouth = []
x_mouth = []
y_mouth = []
w_mouth = []
h_mouth = []


#If there are exactly 2 faces, 4 eys and 2 noses

if (len(faces == 2) and (len(leyes) == 4) and (len(noses) == 2) and (len(mouths) == 2) ):
    for(x, y, w, h) in faces:
        crop_image = frame[y:y+h, x:x+w]
        #detect eyes
        

        for(x1, y1, w1, h1) in leyes:

            if(x1 > x) and (y1 > y) and (x1 < (x + w)) and (y1 < (y + h)):
                crop_image = frame[y1:y1+h1, x1:x1+w1]
                eyes_1.append(crop_image)
                x_eyes.append(x1)
                y_eyes.append(y1)
                h_eyes.append(h1)
                w_eyes.append(w1)
        #detect noses
        for(x1, y1, w1, h1) in noses:
            if(x1 > x) and (y1 > y) and (x1 < (x + w)) and (y1 < y + h):
                crop_image = frame[y1:y1+h1, x1:x1+w1]
                nose.append(crop_image)
                x_nose.append(x1)
                y_nose.append(y1)
                h_nose.append(h1)
                w_nose.append(w1)

        #detect mouths
        for(x1, y1, w1, h1) in mouths:
            if(x1 > x) and (y1 > y) and (x1 < (x + w)) and (y1 < y + h):
                crop_image = frame[y1:y1+h1, x1:x1+w1]
                mouth.append(crop_image)
                x_mouth.append(x1)
                y_mouth.append(y1)
                h_mouth.append(h1)
                w_mouth.append(w1)


    #swap eyes
    if(abs(x_eyes[0] - x_eyes[2])) < (abs(x_eyes[0] - x_eyes[3])):
        rescaledeyes0 = cv2.resize(eyes_1[0], (w_eyes[2], h_eyes[2]), interpolation = cv2.INTER_CUBIC)
        rescaledeyes1 = cv2.resize(eyes_1[1], (w_eyes[3], h_eyes[3]), interpolation = cv2.INTER_CUBIC)
        rescaledeyes2 = cv2.resize(eyes_1[2], (w_eyes[0], h_eyes[0]), interpolation = cv2.INTER_CUBIC)
        rescaledeyes3 = cv2.resize(eyes_1[3], (w_eyes[1], h_eyes[1]), interpolation = cv2.INTER_CUBIC)

        frame[y_eyes[2]:w_eyes[2] + y_eyes[2], x_eyes[2]:w_eyes[2] + x_eyes[2]] = rescaledeyes0
        frame[y_eyes[3]:y_eyes[3] + w_eyes[3], x_eyes[3]:w_eyes[3] + x_eyes[3]] = rescaledeyes1
        frame[y_eyes[0]:y_eyes[0] + w_eyes[0], x_eyes[0]:w_eyes[0] + x_eyes[0]] = rescaledeyes2
        frame[y_eyes[1]:y_eyes[1] + w_eyes[1], x_eyes[1]:w_eyes[1] + x_eyes[1]] = rescaledeyes3
    else:
        rescaledeyes0 = cv2.resize(eyes_1[0], (w_eyes[3], h_eyes[3]), interpolation = cv2.INTER_CUBIC)
        rescaledeyes1 = cv2.resize(eyes_1[1], (w_eyes[2], h_eyes[2]), interpolation = cv2.INTER_CUBIC)
        rescaledeyes2 = cv2.resize(eyes_1[2], (w_eyes[1], h_eyes[1]), interpolation = cv2.INTER_CUBIC)
        rescaledeyes3 = cv2.resize(eyes_1[3], (w_eyes[0], h_eyes[0]), interpolation = cv2.INTER_CUBIC)

        frame[y_eyes[3]:w_eyes[3] + y_eyes[3], x_eyes[3]:w_eyes[3] + x_eyes[3]] = rescaledeyes0
        frame[y_eyes[2]:y_eyes[2] + w_eyes[2], x_eyes[2]:w_eyes[2] + x_eyes[2]] = rescaledeyes1
        frame[y_eyes[1]:y_eyes[1] + w_eyes[1], x_eyes[1]:w_eyes[1] + x_eyes[1]] = rescaledeyes2
        frame[y_eyes[0]:y_eyes[0] + w_eyes[0], x_eyes[0]:w_eyes[0] + x_eyes[0]] = rescaledeyes3

    #swap nose
    rescalednose0 = cv2.resize(nose[0], (w_nose[1], h_nose[1]), interpolation = cv2.INTER_CUBIC)
    rescalednose1 = cv2.resize(nose[1], (w_nose[0], h_nose[0]), interpolation = cv2.INTER_CUBIC)
    frame[y_nose[1]:h_nose[1] + y_nose[1], x_nose[1]:w_nose[1] + x_nose[1]] = rescalednose0
    frame[y_nose[0]:h_nose[0] + y_nose[0], x_nose[0]:w_nose[0] + x_nose[0]] = rescalednose1

    #swap mouth
    print(len(mouth))
    rescaledmouth0 = cv2.resize(mouth[0], (w_mouth[1], h_mouth[1]), interpolation = cv2.INTER_CUBIC)
    rescaledmouth1 = cv2.resize(mouth[1], (w_mouth[0], h_mouth[0]), interpolation = cv2.INTER_CUBIC)
    frame[y_mouth[1]:h_mouth[1] + y_mouth[1], x_mouth[1]:w_mouth[1] + x_mouth[1]] = rescaledmouth0
    frame[y_mouth[0]:h_mouth[0] + y_mouth[0], x_mouth[0]:w_mouth[0] + x_mouth[0]] = rescaledmouth1
        


'''





    rescaledeyes0 = cv2.resize(eyes_1[1], (w_eyes[2], h_eyes[2]), interpolation = cv2.INTER_CUBIC)
    rescaledeyes1 = cv2.resize(eyes_1[0], (w_eyes[3], h_eyes[3]), interpolation = cv2.INTER_CUBIC)
    rescaledeyes2 = cv2.resize(eyes_1[3], (w_eyes[0], h_eyes[0]), interpolation = cv2.INTER_CUBIC)
    rescaledeyes3 = cv2.resize(eyes_1[2], (w_eyes[1], h_eyes[1]), interpolation = cv2.INTER_CUBIC)
    rescalednose0 = cv2.resize(nose[0], (w_nose[1], h_nose[1]), interpolation = cv2.INTER_CUBIC)
    rescalednose1 = cv2.resize(nose[1], (w_eyes[0], h_eyes[0]), interpolation = cv2.INTER_CUBIC)
    
    frame[y_eyes[2]:w_eyes[2] + y_eyes[2], x_eyes[2]:w_eyes[2] + x_eyes[2]] = rescaledeyes0
    #frame[y_eyes[3]:y_eyes[3] + w_eyes[3], x_eyes[3]:w_eyes[3] + x_eyes[3]] = rescaledeyes1
    
'''
        
        

        



'''
xPos = []
yPos = []
width = []
height = []
    # detect if there are two faces in the camera
if len(faces) == 2:
    for(x, y, w, h) in faces:
        crop_image = frame[y:y+h, x:x+w]
        crop_images.append(crop_image)
        xPos.append(x)
        yPos.append(y)
        width.append(w)
        height.append(h)

    # when ever there are two faces in camera, get size of each faces
if len(crop_images) == 2:
        #image0 = Image.fromarray(crop_images[0])
        #print(type(crop_images[0]))
        #width0, height0 = crop_images[0].shape[:2]
        #width1, height1 = crop_images[1].shape[:2]
    rescaledFace0 = cv2.resize(crop_images[0], (width[1], height[1]), interpolation = cv2.INTER_CUBIC)
    rescaledFace1 = cv2.resize(crop_images[1], (width[0], height[0]), interpolation = cv2.INTER_CUBIC)


    #frame[yPos[1]:width[1] + yPos[1], xPos[1]:width[1] + xPos[1]] = rescaledFace0
    #frame[yPos[0]:yPos[0] + width[0], xPos[0]:width[0] + xPos[0]] = rescaledFace1

    # draw the rectangle indicating face on the original frame
    for(x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 3)
    for(x, y, w, h) in leyes:
        frame = cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 3)
    for(x, y, w, h) in nose:
        frame = cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 3)

'''

cv2.namedWindow('frame',cv2.WINDOW_NORMAL)
cv2.resizeWindow('frame', 600,600)
cv2.imshow('frame', frame)
cv2.waitKey(0)
cv2.destroyAllWindows()