import cv2

cap = cv2.VideoCapture(0)

# get the model for face detection
face_detection = cv2.CascadeClassifier('resources\\haarcascade_frontalface_default.xml')

square = True

while True:
    ret, frame = cap.read()

    # turn the frame to gray
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # get the face position from the grey image
    faces = face_detection.detectMultiScale(gray, 1.1, 5)

    crop_images = []
    xPos = []
    yPos = []
    width = []
    height = []
    # detect if there are two faces in the camera
    if len(faces) == 2:
        for(x, y, w, h) in faces:
            crop_image = frame[y:y+h, x:x+w]
            crop_images.append(crop_image)
            xPos.append(x)
            yPos.append(y)
            width.append(w)
            height.append(h)

    # when ever there are two faces in camera, get size of each faces
    if len(crop_images) == 2:
        # resize the two faces in camera
        rescaledFace0 = cv2.resize(crop_images[0], (width[1], height[1]), interpolation=cv2.INTER_CUBIC)
        rescaledFace1 = cv2.resize(crop_images[1], (width[0], height[0]), interpolation=cv2.INTER_CUBIC)

        # update the frame to swap faces
        frame[yPos[1]:width[1] + yPos[1], xPos[1]:width[1] + xPos[1]] = rescaledFace0
        frame[yPos[0]:yPos[0] + width[0], xPos[0]:width[0] + xPos[0]] = rescaledFace1

    # draw the rectangle indicating face on the original frame
    if square:
        for(x, y, w, h) in faces:
            frame = cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 3)

    # show the frame that is converted to gray
    cv2.imshow('frame', frame)

    # press 's' to get rid of square or to enable square around face
    stroke = cv2.waitKey(1)
    if stroke & 0xFF == ord('s'):
        print("s stroked")
        square = not square

    # wait for a 'q' key press to end video capturing
    if stroke & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
